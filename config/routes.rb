Rails.application.routes.draw do
  get    'dashboard'           => 'dashboard#index'
  get    'join'                => 'join#new'
  post   'join'                => 'join#create'

  get    'slack'               => 'slack#new'
  post   'slack'               => 'slack#notifier'

  resources :comments, only: [:create]

  resources :request_acceptants, only: [:edit]

  get    'posts/:id/comments'  => 'posts#comments'
  resources :posts, only: [:create, :destroy]

  namespace :admin do
    get    'projects/list_users' => 'projects#list_users'
    post   'projects/add'        => 'projects#add'
    post   'projects/pdf'        => 'projects#pdf'
    delete 'projects/remove'     => 'projects#remove'
    resources :projects

    resources :requests, only: [:index, :create, :destroy]
    post 'requests/pdf'    => 'requests#pdf'
    get 'requests/sent'    => 'requests#sent'
    get 'requests/pending' => 'requests#pending'
  end

  resources :projects, only: [:index, :show]

  devise_for :users, controllers: { registrations: 'users/registrations' }

  resources :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  authenticated :user do
    root :to => 'dashboard#index', :as => :authenticated_root
  end
  root 'homepage#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end



  match '*path', to: 'errors#routing', via: [:get]
end

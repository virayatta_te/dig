class AddUserTypeIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_type_id, :integer, null: false
  end
end

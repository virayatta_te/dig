class AddAcceptantToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :acceptant_digest, :string
    add_column :requests, :accepted, :boolean, default: false
    add_column :requests, :accepted_at, :datetime

    remove_column :requests, :status
  end
end

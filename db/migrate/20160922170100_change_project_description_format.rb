class ChangeProjectDescriptionFormat < ActiveRecord::Migration
  def change
    change_column :projects, :description, :text, :limit => 65535
  end
end

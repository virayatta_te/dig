class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :file
      t.string :status, default: "Pending"
      t.integer :user_type_id, null: false

      t.timestamps null: false
    end
  end
end

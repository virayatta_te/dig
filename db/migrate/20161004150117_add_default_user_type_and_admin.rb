class AddDefaultUserTypeAndAdmin < ActiveRecord::Migration
  def up
    user_type = UserType.create(id: 1, user_type: "Admin")
    user_type.users.create!(name: "Dig Admin", email: "admin@dig.com", password: "Password1",
      password_confirmation: "Password1", phone: "0431 647 312")

    UserType.create(user_type: "Client")
    UserType.create(user_type: "Developer")
  end

  def down
    UserType.find(1).users.destroy_all
    UserType.find(1).destroy
  end
end

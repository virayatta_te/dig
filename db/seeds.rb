# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Request.destroy_all

10.times do |n|
  name  = Faker::Name.name
  file = "#{Rails.root}/spec/resources/images/attachment.pdf"
  email = Faker::Internet.free_email
  phone = "#{Faker::PhoneNumber.cell_phone}"

  client_user_type = UserType.find_by_user_type "Client"
  request = client_user_type.requests.create(id: n+1,
                                             name:  name,
                                             email: email,
                                             phone: phone)

  uploader = FileUploader.new(request, :file)
  uploader.store!(File.open(file))
  request.file = uploader
  request.save
end

10.times do |n|
  name  = Faker::Name.name
  file = "#{Rails.root}/spec/resources/images/attachment.pdf"
  email = Faker::Internet.free_email
  phone = "#{Faker::PhoneNumber.cell_phone}"

  developer_user_type = UserType.find_by_user_type "Developer"
  request = developer_user_type.requests.create(id: n+11,
                                                name:  name,
                                                email: email,
                                                phone: phone)

  uploader = FileUploader.new(request, :file)
  uploader.store!(File.open(file))
  request.file = uploader
  request.save
end

User.where.not(id: 1).destroy_all

10.times do |n|
  name  = Faker::Name.name
  file = "#{Rails.root}/spec/resources/images/attachment.pdf"
  email = Faker::Internet.free_email
  phone = "#{Faker::PhoneNumber.cell_phone}"

  client_user_type = UserType.find_by_user_type "Client"
  request = client_user_type.users.create(id: n+2,
                                          name:  name,
                                          email: email,
                                          phone: phone,
                                          file: file,
                                          password: "Password1",
                                          password_confirmation: "Password1")

  uploader = FileUploader.new(request, :file)
  uploader.store!(File.open(file))
  request.file = uploader
  request.save
end

10.times do |n|
  name  = Faker::Name.name
  file = "#{Rails.root}/spec/resources/images/attachment.pdf"
  email = Faker::Internet.free_email
  phone = "#{Faker::PhoneNumber.cell_phone}"

  developer_user_type = UserType.find_by_user_type "Developer"
  request = developer_user_type.users.create(id: n+12,
                                             name:  name,
                                             email: email,
                                             phone: phone,
                                             file: file,
                                             password: "Password1",
                                             password_confirmation: "Password1")

  uploader = FileUploader.new(request, :file)
  uploader.store!(File.open(file))
  request.file = uploader
  request.save
end

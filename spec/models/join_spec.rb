require 'rails_helper'

RSpec.describe Request, type: :model do

  let(:file)         { "#{Rails.root}/spec/resources/images/attachment.pdf" }
  let(:name)         { "Test" }
  let(:email)        { "test@test.com" }
  let(:phone)        { "0431647312" }
  let(:user_type_id) { 1 }

  let(:request)      { Request.new(name:         name,
                                   email:        email,
                                   phone:        phone,
                                   user_type_id: user_type_id) }

  let(:uploader)     { FileUploader.new(request, :file) }

  let(:user_type)    { UserType.create(id: 1, user_type: "Admin") }

  before do
    FileUploader.enable_processing = true
    uploader.store!(File.open(file))
    request.file = uploader
    # to fix relationship bugs.
    request.user_type = user_type
  end

  after do
    uploader.remove!
    FileUploader.enable_processing = false
  end

  it 'should successfully saved' do
    expect(request.save).to eq(true)
  end

  it 'should not be saved' do

    # Check when name is null
    request_dup = request.dup
    request_dup.name = ''
    expect(request_dup.save).to eq(false)

    # Check when email is null
    request_dup = request.dup
    request_dup.email = ''
    expect(request_dup.save).to eq(false)

    # Check when phone is null
    request_dup = request.dup
    request_dup.phone = ''
    expect(request_dup.save).to eq(false)

    # Check when user_type_id is null
    request_dup = request.dup
    request_dup.user_type_id = nil
    expect(request_dup.save).to eq(false)

    # Check when file is null
    # This check should always be the last one as file is being removed
    request_dup = request.dup
    request_dup.file.remove!
    expect(request_dup.save).to eq(false)

  end
end

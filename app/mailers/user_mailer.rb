class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.request_acceptant.subject
  #
  def request_acceptant request
    @request = request

    mail to: request.email, subject: "Request acceptant"
  end
end

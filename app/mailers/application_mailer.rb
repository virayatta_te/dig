class ApplicationMailer < ActionMailer::Base
  default from: "noreply@dig.com"
  layout 'mailer'
end

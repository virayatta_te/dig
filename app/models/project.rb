class Project < ActiveRecord::Base
  has_many :relationships, foreign_key: "project_id",
                           dependent:   :destroy

  has_many :users, through: :relationships,  source: :user
  has_many :posts, dependent: :destroy

  mount_uploader :file, FileUploader

  validates :name, presence: true
  validates :description, presence: true
end

class Request < ActiveRecord::Base
  attr_accessor :acceptant_token

  belongs_to :user_type

  mount_uploader :file, FileUploader

  validates :name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :presence => true, :length => { maximum: 255 },
                    :format => { with: VALID_EMAIL_REGEX },
                    :uniqueness => { case_sensitive: false }
  validates :phone, :presence => true
  validates :user_type, :presence => true
  validate :file_exist


  # Returns the hash digest of the given string.
  def Request.digest string
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create string, cost: cost
  end

  # Returns a random token.
  def Request.new_token
    SecureRandom.urlsafe_base64
  end

  # Accepts a user request in the database for sing up
  def accept
    update_columns :accepted => true, :accepted_at => Time.zone.now
  end

  # Returns true if the given token matches the digest
  def authenticated? acceptant_token
    BCrypt::Password.new(acceptant_digest).is_password? acceptant_token
  end

  # Creates and assigns the acceptant token and digest
  def create_acceptant_digest
    self.acceptant_token = Request.new_token
    update_columns acceptant_digest: Request.digest(acceptant_token)
  end

  # Sends activatation email.
  def send_acceptant_email
    UserMailer.request_acceptant(self).deliver_now
  end

  private

    def file_exist
      unless file.present?
        errors.add(:file, "cannot be null")
      end
    end
end

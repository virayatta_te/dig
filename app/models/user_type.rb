class UserType < ActiveRecord::Base
  has_many :users
  has_many :requests

  validates :user_type, :presence => true

  # Default admin name
  Admin = "Admin"
end

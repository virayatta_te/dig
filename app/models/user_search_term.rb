class UserSearchTerm
  attr_reader :where_clause, :where_args, :order

  def initialize search_term, user_type
    search_term = search_term.downcase
    @where_clause = String.new
    @where_args = Hash.new

    build_for_email_search search_term, user_type

    # ///
    # /// Enable this is seach email and name separately
    # ///
    # if search_term =~ /@/
    #   build_for_email_search search_term
    # else
    #   build_for_name_search search_term
    # end
  end

  def build_for_name_search search_term
    @where_clause << case_insensitive_search(:name)
    @where_args[:name] = starts_with search_term

    @order = "created_at asc"
  end

  def build_for_email_search search_term, user_type
    @where_clause << "user_type_id != :user_type AND ("
    @where_args[:user_type] = user_type

    @where_clause << case_insensitive_search(:name)
    @where_args[:name] = starts_with extract_name(search_term)

    @where_clause << " OR #{case_insensitive_search :email})"
    @where_args[:email] = starts_with search_term

    @order = "created_at desc, name asc"
  end

  private

    def starts_with search_term
      search_term + "%"
    end

    def case_insensitive_search field_name
      "lower(#{field_name}) like :#{field_name}"
    end

    def extract_name email
      email.gsub(/@.*$/,'').gsub(/[0-9]+/,'')
    end
end

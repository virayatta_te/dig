class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :project

  has_many :comments, dependent: :destroy

  default_scope -> { order(created_at: :desc) }
  mount_uploader :file, PostUploader

  validates :user_id, presence: true
  validates :project_id, presence: true
  validates :content, presence: true
end

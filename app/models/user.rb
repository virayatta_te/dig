class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :user_type
  has_one :user_detail
  has_many :relationships, foreign_key: "user_id",
                           dependent:   :destroy

  has_many :projects, through: :relationships,  source: :project
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :presence => true, :length => { maximum: 255 },
                    :format => { with: VALID_EMAIL_REGEX },
                    :uniqueness => { case_sensitive: false }
  validates :password, :presence => true, :length => { minimum: 6 },
                       :allow_nil => true
  validates :user_type, :presence => true

  def feed
    Post.where("project_id IN (:project_ids)", project_ids: project_ids)
  end

end

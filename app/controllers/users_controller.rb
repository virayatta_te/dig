class UsersController < ApplicationController


  def index
    # Next page is true as default
    next_exist = true

    page = (params[:page] || 0).to_i
    next_exist = find_users(page + 1).count > 0
    @users = find_users page


    respond_to do |format|
        format.html
        format.json { render json: {
          users: @users.as_json(include: { projects: { only: :id } }),
          next_exist: next_exist
        } }
    end
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts
  end

end

class RequestAcceptantsController < ApplicationController
  skip_before_action :authenticate_user!
  def edit
    begin
      request = Request.find_by_email params[:email]
      if request.present?
        raise Unauthorised unless request.authenticated? params[:id]
      else
        raise NonPresent
      end
    rescue
      warning = "Your request must be accepted first before able to sign up."
      redirect_to join_url, alert: warning
    ensure
      redirect_to new_user_registration_url id: request.id, token: params[:id]
    end
  end
end

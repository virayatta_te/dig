class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  POST_SIZE = 4
  PAGE_SIZE = 6
  COMMENT_SIZE = 1

  def admin?
    return current_user ? current_user.user_type.user_type.eql?(UserType::Admin) : false
  end

  private

    def find_users page

      user_type = UserType.find_by_user_type(UserType::Admin)

      if params[:keywords].present?
        keywords = params[:keywords]
        user_search_term = UserSearchTerm.new keywords, user_type
        @users = User.where(
            user_search_term.where_clause,
            user_search_term.where_args).
          order(user_search_term.order).
          offset(PAGE_SIZE * page).limit(PAGE_SIZE)
      else
        @users = User.where.
                  not(user_type_id: user_type).
                  order(:name).
                  offset(PAGE_SIZE * page).limit(PAGE_SIZE)
      end
    end
end

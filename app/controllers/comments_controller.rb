class CommentsController < ApplicationController
  def create
    @post = Post.find params[:post_id]

    comment = current_user.comments.build comment_params

    @post.comments << comment

    @new_comment = Comment.first

    respond_to do |format|
      format.json { render json: @new_comment.as_json(
        include: { user: { only: [:name, :created_at] } }) }
    end
  end

  private

    def comment_params
      params.require(:comment).permit(:comment)
    end
end

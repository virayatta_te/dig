class DashboardController < ApplicationController

  def index
    exist = false;
    page = (params[:page] || 0).to_i
    if admin?
      exist = Post.all.
                offset(POST_SIZE * (page+1)).limit(1).count > 0
      @feed = Post.all.
                offset(POST_SIZE * page).limit(POST_SIZE)
    else
      exist = current_user.feed.
                offset(POST_SIZE * (page+1)).limit(1).count > 0
      @feed = current_user.feed.
                offset(POST_SIZE * page).limit(POST_SIZE)
    end
    respond_to do |format|
      format.html
      format.json { render json: { posts: @feed.as_json(
        include: { user: { only: :name }, project: { only: :name } }),
        more_exist: exist } }
    end
  end
end

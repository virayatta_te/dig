class PostsController < ApplicationController

  def create
    project = Project.find params[:project_id]

    post = current_user.posts.build post_params

    project.posts << post

    @new_post = Post.first

    respond_to do |format|
      format.json { render json: @new_post.as_json(
        include: { user: { only: :name }, project: { only: :name } }) }
    end

  end

  def destroy
  end

  def comments
    page = (params[:page] || 0).to_i

    @post = Post.find params[:id]
    @more_comment_exist = Comment.where(post_id: @post.id).
                  offset(COMMENT_SIZE * (page+1)).limit(1).count > 0
    @comments = Comment.where(post_id: @post.id).
                  offset(COMMENT_SIZE * page).limit(COMMENT_SIZE)

    @count = Comment.where(post_id: @post.id).count

    respond_to do |format|
      format.json { render json: @post.as_json(only: :id).
        merge(comments: @comments.as_json(
          include: {
            user: { only: [:name, :created_at]}
          }
        )).
        merge(count: @count).
        merge(more_comment_exist: @more_comment_exist) }
    end
  end

  private

    def post_params
      params.require(:post).permit(:content, :file)
    end
end

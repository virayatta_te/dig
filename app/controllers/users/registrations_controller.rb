class Users::RegistrationsController < Devise::RegistrationsController
# before_action :configure_account_update_params, only: [:update]
before_action :configure_sign_up_params, only: [:create]
before_action :find_user_type_id, only: [:create]
before_action :is_from_accepted_request, only: [:new]

  # GET /resource/sign_up
  def new
    @user_type = UserType.readonly.map(&:user_type)
    super
  end

  # POST /resource
  def create
    @user = User.new user_params
    if @request = Request.where(accepted: true).find_by_email(params[:user][:email])
      @user.name = @request.name
      @user.phone = @request.phone
      @user.file = @request.file

      if @user.save
        @request.destroy
        sign_in(@user)
        set_flash_message! :notice, :signed_up
        redirect_to root_url
      else
        redirect_to request.referer, alert: 'Something wrong!'
      end
    else
      redirect_to request.referer, alert: 'Contact administrator for help'
    end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_type_id])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :user_type_id)
    end

    def redirect_from_email_params
      params.permit(:id, :token)
    end

    def find_user_type_id
      param = params.require(:user).permit(:user_type)
      user_type = UserType.find_by_user_type param[:user_type]

      unless user_type.present?
        user_type_id = nil
      else
        user_type_id = user_type.id
      end

      params[:user][:user_type_id] = user_type_id
      params[:user].delete(:user_type)
    end

    def is_from_accepted_request
      begin
        if redirect_from_email_params.count.eql? 2
          request = Request.find redirect_from_email_params[:id]
          raise Unauthorised unless request
          raise Unauthorised unless request.authenticated? redirect_from_email_params[:token]
        else
          raise NonPresent
        end
      rescue
        warning = "Invalid request!"
        redirect_to join_url, alert: warning
      end
    end
end

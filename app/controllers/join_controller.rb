class JoinController < ApplicationController
  skip_before_action :authenticate_user!

  def new
    @request = Request.new
    @user_type = UserType.readonly.map(&:user_type)
  end

  def create
    @request = Request.new request_params
    @user_type = UserType.readonly.map(&:user_type)

    begin
      if @request.save
        flash[:success] = "Request sent!"
        redirect_to root_url
      else
        render :new
      end
    rescue
      render :new
    end
  end

  private

    def request_params
      request = params.require(:request).permit(:name, :email, :phone, :file, :user_type)
      request[:user_type_id] = find_by_user_type request[:user_type]
      request.delete(:user_type)
      request
    end

    def find_by_user_type param
      user_type = UserType.find_by_user_type(param)

      unless user_type.present?
        user_type_id = nil
      else
        user_type_id = user_type.id
      end
    end
end

class Admin::ApplicationController < ApplicationController
  before_filter :verify_is_admin

  private

    def verify_is_admin
      if current_user.user_type.user_type.eql? UserType::Admin
        return
      else
        redirect_to root_url
      end
    end
end

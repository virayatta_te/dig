class Admin::ProjectsController < Admin::ApplicationController

  def index
    @projects = Project.all
  end

  def new
    @project = Project.new
  end

  def edit
    @project = Project.find params[:id]
  end

  def update
    @project = Project.find params[:id]
    if @project.update_attributes(project_params)
      redirect_to admin_project_url(@project), notice: "Project updated!"
    else
      redirect_to admin_project_url(@project), alert: "Something wrong, please try again."
    end
  end

  def create
    @project = Project.new project_params
    if @project.save
      flash[:success] = "Request sent!"
      redirect_to root_url
    else
      render :new
    end
  end

  def show
    begin
      @project = Project.find params[:id]
      page = (params[:page] || 0).to_i
      find_users page
      respond_to do |format|
          format.html { render :show }
      end
    rescue
      redirect_to root_url
    end
  end

  def add
    user = User.find params[:user_id]
    project = Project.find params[:project_id]

    user.relationships.create project_id: project.id

    @results = project.users

    respond_to do |format|
        format.html
        format.json { render json: @results.to_json(
          include: { projects: { only: :id } }) }
    end
  end

  def remove
    user = User.find params[:user_id]
    project = Project.find params[:project_id]

    user.relationships.find_by_project_id(project.id).destroy

    @results = project.users

    respond_to do |format|
        format.html
        format.json { render json: @results.to_json(
          include: { projects: { only: :id } }) }
    end
  end

  def list_users
    project = Project.find params[:project_id]
    @results = project.users

    respond_to do |format|
        format.html
        format.json { render json: @results.to_json(
          include: { projects: { only: :id } }) }
    end
  end

  def pdf
    project = Project.find(params[:format])

    # File location
    location = project.file.file.file
    filename = "project_" + project.id.to_s + "_attachment.pdf"

    send_file(location, :filename => filename, :type => "application/pdf")
  end

  private
  def project_params
    request = params.require(:project).permit(:name, :description, :file)
  end
end

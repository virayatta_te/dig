class Admin::RequestsController < Admin::ApplicationController


  def index
    @requests_pending = Request.where(accepted: false).
                                paginate(page: params[:pending_page], per_page: 5)
    @requests_sent    = Request.where(accepted: true).
                                paginate(page: params[:page_sent], per_page: 2)
  end

  def sent
    if params[:filter]
      user_type_id = UserType.find_by_user_type(params[:filter]).id
      @requests = Request.where(accepted: true, user_type_id: user_type_id).
                                paginate(page: params[:page], per_page: 10)
    else
      @requests = Request.where(accepted: true).
                                paginate(page: params[:page], per_page: 10)
    end
  end

  def pending
    if params[:filter]
      user_type_id = UserType.find_by_user_type(params[:filter]).id
      @requests = Request.where(accepted: false, user_type_id: user_type_id).
                                paginate(page: params[:page], per_page: 10)
    else
      @requests = Request.where(accepted: false).
                                paginate(page: params[:page], per_page: 10)
    end
  end

  def create
    @request = Request.find params[:format]
    if @request
      @request.create_acceptant_digest
      @request.send_acceptant_email
      @request.accept
      @requests = Request.all
      redirect_to request.referer, notice: "Email sent"
    else
      redirect_to request.referer, alert: "Somthing wrong"
    end
  end

  def destroy
    Request.find(params[:id]).destroy

    @requests = Request.all
    redirect_to request.referer, notice: "User deleted"
  end

  def pdf
    request = Request.find(params[:format])

    # File location
    location = request.file.file.file
    filename = "request_" + request.id.to_s + "_attachment.pdf"

    send_file(location, :filename => filename, :type => "application/pdf")
  end
end

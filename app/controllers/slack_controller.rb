class SlackController < ApplicationController
  skip_before_action :authenticate_user!

  def new
  end

  def notifier
    cmd = %{curl -X POST -H 'Content-type: application/json'
      --data '{"text":"This is a line of text.\nAnd this is another one."}'
      https://hooks.slack.com/services/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}

    IO.popen cmd
  end
end

class ProjectsController < ApplicationController

  def index
    @projects = current_user.projects
  end

  def show
    page = (params[:page] || 0).to_i
    @post = Post.new
    @project = Project.find params[:id]

    exist = @project.posts.
              offset(POST_SIZE * (page+1)).limit(1).count > 0
    @posts = @project.posts.
              offset(POST_SIZE * page).limit(POST_SIZE)

    respond_to do |format|
      format.html
      format.json { render json: { posts: @posts.as_json(
        include: { user: { only: :name }, project: { only: :name } }),
        more_exist: exist } }
    end
  end
end

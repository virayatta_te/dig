// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require 'bootstrap-sass-official'
//= require jquery_ujs
//= require angular
//= require ng-timeago
//= require_tree .

angular.module('members', ['ngtimeago'])
.config([
  "$httpProvider", function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
  }
])
.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}])
.controller("MembersSearchController", [
  "$scope", "$http",
  function($scope, $http) {

    $scope.page = 0;
    $scope.next_exist = '';

    var keywords = "";

    // Get current project //
    $scope.project = {}

    if (document.getElementById('div-project-data') != null) {
      $scope.project = { id: document.getElementById('div-project-data').getAttribute("data-project-id") }
    }

    $scope.exist = function(user, project) {
      return user.projects.some(function(p) {
        return p.id == project.id
      });
    };
    // END //

    $scope.members = [];
    $scope.search = function(searchTerm, all) {

      if (!all && searchTerm.length == 0) {
        $scope.page = 0;
      }

      keywords = searchTerm;

      $http.get("/users.json",
                {  "params": { "keywords": searchTerm, "page": $scope.page } }
      ).then(function(response) {
          $scope.members = response.data.users;
          $scope.next_exist = response.data.next_exist;
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.results = [];

    $scope.init_results = function(project) {
      $http.get("/admin/projects/list_users.json",
                {  "params": { "project_id": project.id } }
      ).then(function(response) {
          $scope.results = response.data;
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.add = function(user, project, searchTerm) {

      $http.post("/admin/projects/add.json",
                { "user_id": user.id, "project_id": project.id }
      ).then(function(response) {
          $scope.results = response.data;
          $scope.search(keywords, 'false');
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.remove = function(user, project, searchTerm) {

      $http.delete("/admin/projects/remove.json",
                  { "params": { "user_id": user.id, "project_id": project.id } }
      ).then(function(response) {
          $scope.results = response.data;
          $scope.search(keywords, 'false');
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }


    $scope.previousPage = function() {
      if ($scope.page > 0) {
        $scope.page = $scope.page - 1;
        $scope.search($scope.keywords, 'false')
      }
    }

    $scope.nextPage = function() {
      $scope.page = $scope.page + 1;
      $scope.search($scope.keywords, 'false');
    }
  }
])
.controller("PostsController", [
  "$scope", "$http",
  function($scope, $http) {

    $scope.date = new Date()

    $scope.m_page = 0;
    $scope.posts = [];

    $scope.file = "";

    $scope.more_exist = '';

    $scope.more_func = function(project) {
      var url = ''
      if (project) {
        url = "/projects/" + project.id + ".json";
      }
      else {
        url = "/dashboard.json";
      }

      $http.get(url, {  "params": { "page": $scope.m_page } }
      ).then(function(response) {
          $scope.posts.push.apply($scope.posts, response.data.posts);
          $scope.more_exist = response.data.more_exist;
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.post = function(content, file, project) {
      $http.post("/posts.json",
        { "project_id": project.id,
          "post": { "content": content, "file": file } }
      ).then(function(response) {
          $scope.posts.unshift(response.data);
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.more_page = function(project) {
      $scope.m_page = $scope.m_page + 1;
      $scope.more_func(project);
    }

    var c_pages = [];
    $scope.comments = [];
    $scope.counts = [];
    $scope.more_comment_exist = [];

    $scope.get_comment = function(post) {
      c_pages[post.id] = c_pages[post.id] || 0;

      $http.get("/posts/" + post.id + "/comments.json",
        {  "params": { "page": c_pages[post.id] } }
      ).then(function(response) {
        $scope.comments[response.data.id] = $scope.comments[response.data.id] || [];
        $scope.comments[response.data.id].push.apply($scope.comments[response.data.id], response.data.comments)

        $scope.counts[response.data.id] = response.data.count

        $scope.more_comment_exist[response.data.id] = response.data.more_comment_exist;
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.add_comment = function(post, comment) {
      $http.post("/comments.json",
        { "post_id" : post.id ,
          "comment": { "comment": comment } }
      ).then(function(response) {
          $scope.comments[post.id].unshift(response.data);
          $scope.counts[post.id] += 1;
      },function(response) {
          alert("There was a problem: " + response.status);
        }
      );
    }

    $scope.more_comment = function(post) {
      c_pages[post.id] = c_pages[post.id] + 1
      $scope.get_comment(post);
    }
  }
]);








$(function() {
   $('#flash').delay(500).fadeIn('slow', function() {
      $(this).delay(1500).fadeOut('slow');
   });
});

$(document).ready(function(){
    $('#join-popover').popover();
});

// function refreshPage () {
//     var page_y = document.getElementsByTagName("body")[0].scrollTop;
//     window.location.href = window.location.href.split('?')[0] + '?page_y=' + page_y;
// }
// window.onload = function () {
//     setTimeout(refreshPage, 35000);
//     if ( window.location.href.indexOf('page_y') != -1 ) {
//         var match = window.location.href.split('?')[1].split("&")[0].split("=");
//         document.getElementsByTagName("body")[0].scrollTop = match[1];
//     }
// }


$(document).ready(function() {

	// For the Second level Dropdown menu, highlight the parent
	$( ".dropdown-menu" )
	.mouseenter(function() {
		$(this).parent('li').addClass('active');
	})
	.mouseleave(function() {
		$(this).parent('li').removeClass('active');
	});

});

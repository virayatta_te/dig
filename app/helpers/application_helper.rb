module ApplicationHelper
  def admin?
    current_user.user_type.user_type.eql?(UserType::Admin)
  end
end

source 'https://rubygems.org'

# Use pdf-reader to reader file attachment
gem 'pdf-reader',               '~> 1.4'
# Use devise to simplify the setup and initial configuration
gem 'devise'
# Use bower-rails to manage our dependencies
gem 'bower-rails'
# Use faker to generate fake request
gem 'faker',                    '1.4.2'
# Use will_paginate to perform paginated queriers with ActiveRecord
gem 'will_paginate',            '3.0.7'
gem 'bootstrap-will_paginate',  '~> 0.0.10'
# Use CarrierWave to create an file uploader
gem 'carrierwave'
# Use mini_magick and fog gems needed for image resizing and image upload in production
gem 'mini_magick',              '4.5.1'
gem 'fog',                      '1.38.0'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails',                    '4.2.6'
# Use mysql2 as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails',               '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier',                 '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails',             '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder',                 '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc',                     '~> 0.4.0', group: :doc

# Use for a password hashing functioning
gem 'bcrypt',                   '3.1.11'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Use RSpec for testing
  gem "rspec-rails",            '~> 3.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console',            '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Use railroady to generate model and controller diagrams
  gem 'railroady'
end

group :test do
  # Use DatabaseCleaner to reset the database to a pristine state without using transactions
  gem 'database_cleaner'

  # Driver for Capybara that allows you to run your tests on a headless WebKit browser, provided by PhantomJS
  gem 'poltergeist'
end
